const { db } = require("./db");
const { getRetentionTestsSchedule } = require("./domain/schedule");
const {
  INSERT_NOTIFICATION,
  SET_NOTIFICATION_AS_SENDED,
} = require("./graphql/mutations");
const { GET_NOTIFICATIONS_FROM } = require("./graphql/queries");
const { logger } = require("./logger");

const scheduleNotification = async ({ id: userId }, date, text) => {
  try {
    await db.request(INSERT_NOTIFICATION, {
      userId,
      date,
      text,
    });
    return true;
  } catch (error) {
    logger.error(error.message);
    return false;
  }
};

const scheduleNotifications = async ({ id }, text, zeroDay) => {
  const schedule = getRetentionTestsSchedule(zeroDay);
  schedule.forEach(async (scheduleItem) => {
    try {
      await scheduleNotification({ id }, scheduleItem.date, text);
    } catch (error) {
      logger.error(error);
    }
  });
  return true;
};

const getNotificationsFrom = async (from, to) => {
  const { notifications } = await db.request(GET_NOTIFICATIONS_FROM, {
    from,
    now: to,
  });
  return notifications;
};

const setNotificationAsSended = async (id) => {
  await db.request(SET_NOTIFICATION_AS_SENDED, {
    notificationId: id,
  });
};

module.exports = {
  scheduleNotification,
  scheduleNotifications,
  getNotificationsFrom,
  setNotificationAsSended,
};
