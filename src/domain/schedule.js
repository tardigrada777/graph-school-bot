const OFFSET_TO_RETENTION = 0.2; // 20%

const getRetentionTestsSchedule = (
  zeroDay,
  periodsToRetention = [3, 7, 30]
) => {
  const start = new Date(zeroDay);
  return periodsToRetention.map((period) => {
    const hoursToRetention = period * OFFSET_TO_RETENTION * 24;
    return {
      date: new Date(start.setHours(start.getHours() + hoursToRetention)),
    };
  });
};

module.exports = { getRetentionTestsSchedule };
