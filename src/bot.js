const TeleBot = require('telebot');
const cron = require('node-cron');

const { logger } = require('./logger');
const { getEnvVarOrThrowError, parseQuery } = require('./utils');
const {
  scheduleNotifications,
  getNotificationsFrom,
  setNotificationAsSended,
} = require('./notifications');

const bot = new TeleBot(getEnvVarOrThrowError('BOT_TOKEN'));

bot.on('/echo', (msg) => {
  return msg.reply.text("I'm alive!");
});

bot.on('callbackQuery', async ({ message, from, data }) => {
  const { type, payload: chunkId } = parseQuery(data);
  if (type !== 'select_chunk') return;
  const chunks = require('./data/chunks');
  const chunk = chunks.find((c) => c.id === chunkId);
  if (!chunk) return;
  const notificationText = `${from.first_name}, напоминаю, в течение 12 часов тебе необходимо пройти retention тест порции ${chunk.title}. Ссылка на тест: ${chunk.link}`;
  const result = await scheduleNotifications(
    { id: from.id },
    notificationText,
    new Date()
  );
  bot.deleteMessage(message.chat.id, message.message_id);
  if (result) {
    logger.info(`Send message to ${from.id}, with name: ${from.first_name}`);
    bot.deleteMessage(message.chat.id, message.message_id);
    return bot.sendMessage(
      from.id,
      `Отлично, ${from.first_name}, тесты запланированы!`
    );
  }
  return bot.sendMessage(
    from.id,
    `${from.first_name}, к сожалению что-то пошло не так, пожалуйста попробуй запланировать тесты позже...`
  );
});

bot.on('/learned', async (msg) => {
  // get chunks
  const chunks = require('./data/chunks');

  // fill list of buttons for every chunk, use id as callback
  const options = {
    replyMarkup: bot.inlineKeyboard([
      ...chunks.map((chunk) => {
        return [
          bot.inlineButton(chunk.title, {
            callback: `select_chunk#${chunk.id}`,
          }),
        ];
      }),
    ]),
  };
  return bot.sendMessage(msg.from.id, 'Какую порцию ты изучил?', options);
});

bot.start();

cron.schedule('* * * * *', async () => {
  logger.info(`Cron at ${new Date().toUTCString()}`);
  const hourAgo = new Date(new Date().setHours(new Date().getHours() - 1));
  try {
    const notifications = await getNotificationsFrom(
      hourAgo,
      new Date().toISOString()
    );
    notifications.forEach(async ({ id, target_id, text }) => {
      bot.sendMessage(target_id, text);
      await setNotificationAsSended(id);
    });
    logger.info(
      `Sended ${
        notifications.length
      } notifications at ${new Date().toUTCString()}`
    );
  } catch (error) {
    logger.error(`Error in the cron task: ${error.message}`);
  }
});
