const { gql } = require("graphql-request");

const GET_NOTIFICATIONS_FROM = gql`
  query getNotificationsFrom($from: timestamptz!, $now: timestamptz!) {
    notifications(
      where: { date: { _gte: $from, _lte: $now }, sended: { _eq: false } }
      order_by: { date: asc }
    ) {
      id
      target_id
      date
      text
    }
  }
`;

module.exports = {
  GET_NOTIFICATIONS_FROM,
};
