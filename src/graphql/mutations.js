const { gql } = require("graphql-request");

const INSERT_NOTIFICATION = gql`
  mutation insertNotification(
    $userId: bigint!
    $date: timestamptz!
    $text: String!
  ) {
    insert_notifications_one(
      object: { target_id: $userId, date: $date, text: $text }
    ) {
      id
    }
  }
`;

const SET_NOTIFICATION_AS_SENDED = gql`
  mutation setNotificationAsSended($notificationId: bigint!) {
    update_notifications_by_pk(
      _set: { sended: true }
      pk_columns: { id: $notificationId }
    ) {
      id
    }
  }
`;

module.exports = {
  INSERT_NOTIFICATION,
  SET_NOTIFICATION_AS_SENDED,
};
