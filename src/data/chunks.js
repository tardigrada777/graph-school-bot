const { nanoid } = require('nanoid');

// FIXME: remove, this data is hardcoded because of creating MVP
// remove and replace with getting data from the db
module.exports = [
  {
    id: nanoid(),
    title: 'Арифметические операции',
    link: 'https://graph-school-tester.herokuapp.com/test/2',
  },
  {
    id: nanoid(),
    title: 'Рациональные числа',
    link: 'https://graph-school-tester.herokuapp.com/test/4',
  },
  {
    id: nanoid(),
    title: 'Степень',
    link: 'https://graph-school-tester.herokuapp.com/test/6',
  },
  {
    id: nanoid(),
    title: 'Корень',
    link: 'https://graph-school-tester.herokuapp.com/test/8',
  },
  {
    id: nanoid(),
    title: 'Логарифмы',
    link: 'https://graph-school-tester.herokuapp.com/test/10',
  },
];
