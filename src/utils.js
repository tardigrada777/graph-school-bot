const getEnvVarOrThrowError = (variable) => {
  const v = process.env[variable];
  if (!v)
    throw new Error(
      `Invalid variable name. Variable with name: ${variable} not found!`
    );
  return v;
};

const n = (array, i) => array[i];

const first = (array) => n(array, 0);

const parseQuery = (query = '') => {
  const parts = query.split('#');
  const type = first(parts);
  const payload = n(parts, 1);
  return { type, payload };
};

module.exports = { getEnvVarOrThrowError, parseQuery };
