const { GraphQLClient } = require("graphql-request");

const DB_URL = "https://graph-school-experiment.herokuapp.com/v1/graphql";

const db = new GraphQLClient(DB_URL);

module.exports = { db };
